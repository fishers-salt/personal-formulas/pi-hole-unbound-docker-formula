# frozen_string_literal: true

describe docker.containers do
  its('names') { should_not include 'pi-hole-unbound' }
  its('images') { should_not include 'pi-hole-unbound:latest' }
end
