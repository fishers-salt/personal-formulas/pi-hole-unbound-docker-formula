# frozen_string_literal: true

control 'pi-hole-unbound-docker-setup-vars-absent' do
  title 'should not exist'

  describe file('/srv/docker/pi-hole-unbound/unbound/setupVars.conf') do
    it { should_not exist }
  end
end

control 'pi-hole-unbound-docker-dnsmasq-dir-absent' do
  title 'should not exist'

  describe directory('/srv/docker/pi-hole-unbound/dnsmasq') do
    it { should_not exist }
  end
end

control 'pi-hole-unbound-docker-unbound-dir-absent' do
  title 'should not exist'

  describe directory('/srv/docker/pi-hole-unbound/unbound') do
    it { should_not exist }
  end
end

control 'pi-hole-unbound-docker-dir-absent' do
  title 'should not exist'

  describe directory('/srv/docker/pi-hole-unbound') do
    it { should_not exist }
  end
end
