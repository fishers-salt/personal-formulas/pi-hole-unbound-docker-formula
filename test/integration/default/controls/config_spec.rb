# frozen_string_literal: true

control 'pi-hole-unbound-docker-dir-managed' do
  title 'should exist'

  describe directory('/srv/docker/pi-hole-unbound') do
    it { should exist }
  end
end

control 'pi-hole-unbound-docker-unbound-dir-managed' do
  title 'should exist'

  describe directory('/srv/docker/pi-hole-unbound/unbound') do
    it { should exist }
  end
end

control 'pi-hole-unbound-docker-dnsmasq-dir-managed' do
  title 'should exist'

  describe directory('/srv/docker/pi-hole-unbound/dnsmasq') do
    it { should exist }
  end
end

control 'pi-hole-unbound-docker-setup-vars-managed' do
  title 'should match desired lines'

  describe file('/srv/docker/pi-hole-unbound/unbound/setupVars.conf') do
    it { should be_file }
    its('mode') { should cmp '0644' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') do
      should match(/REV_SERVER_TARGET=\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/)
    end
    its('content') { should include('REV_SERVER_CIDR=172.18.0.0/20') }
  end
end
