# frozen_string_literal: true

describe docker_container(name: 'pihole-unbound') do
  it { should exist }
  it { should be_running }
  its('image') { should eq 'cbcrowe/pihole-unbound:latest' }
  # its('ports') do
  #   should match
  #     %r{53.udp, 53.tcp, 67.udp, 0.0.0.0:8198->80.tcp, 0.0.0.0:d+->3233.tcp}
  # end
  it { should have_volume('/etc/pihole', '/srv/docker/pi-hole-unbound/unbound') }
  it { should have_volume('/etc/dnsmasq.d', '/srv/docker/pi-hole-unbound/dnsmasq') }
end
