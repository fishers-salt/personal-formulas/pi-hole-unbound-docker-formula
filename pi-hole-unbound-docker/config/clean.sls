# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as pi_hole_unbound_docker with context %}

{%- set mountpoint = pi_hole_unbound_docker.docker.mountpoint %}

pi-hole-unbound-docker-setup-vars-absent:
  file.absent:
    - name: {{ mountpoint }}/docker/pi-hole-unbound/unbound/setupVars.conf

pi-hole-unbound-docker-dnsmasq-dir-absent:
  file.absent:
    - name: {{ mountpoint }}/docker/pi-hole-unbound/dnsmasq

pi-hole-unbound-docker-unbound-dir-absent:
  file.absent:
    - name: {{ mountpoint }}/docker/pi-hole-unbound/unbound

pi-hole-unbound-docker-dir-absent:
  file.absent:
    - name: {{ mountpoint }}/docker/pi-hole-unbound
