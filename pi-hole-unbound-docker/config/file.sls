# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_container_running = tplroot ~ '.container.running' %}
{%- from tplroot ~ "/map.jinja" import mapdata as pi_hole_unbound_docker with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_container_running }}

{%- set mountpoint = pi_hole_unbound_docker.docker.mountpoint %}
{%- set user = pi_hole_unbound_docker.container.user %}
{%- set group = pi_hole_unbound_docker.container.group %}

pi-hole-unbound-docker-dir-managed:
  file.directory:
    - name: {{ mountpoint }}/docker/pi-hole-unbound
    - user: root
    - group: root
    - dir_mode: '0755'
    - makedirs: true

pi-hole-unbound-docker-unbound-dir-managed:
  file.directory:
    - name: {{ mountpoint }}/docker/pi-hole-unbound/unbound
    - user: 999
    - group: {{ group }}
    - dir_mode: '0775'
    - makedirs: true
    - require:
      - pi-hole-unbound-docker-dir-managed

pi-hole-unbound-docker-dnsmasq-dir-managed:
  file.directory:
    - name: {{ mountpoint }}/docker/pi-hole-unbound/dnsmasq
    - user: root
    - group: root
    - dir_mode: '0755'
    {# - file_mode: '0644' #}
    - makedirs: true
    {#
    - recurse:
      - mode
      - user
      - group
    #}
    - require:
      - pi-hole-unbound-docker-dir-managed

{#
{%- set network_cidr = pi_hole_unbound_docker['network_cidr'] %}
{%- set gateway = salt.grains.get('ip4_gw', '192.168.0.1') %}
{%- set listen_interface = pi_hole_unbound_docker['listen_interface'] %}
{%- set interfaces = salt.grains.get('ip_interfaces', {}) %}
{%- set addresses = interfaces.get('listen_interface', []) %}
{%- if addresses|length == 0 %}
{%- set address = "test" %}
{%- else %}
{%- set address = addresses[0] %}
{%- endif %}
pi-hole-unbound-docker-setup-vars-managed:
  file.managed:
    - name: {{ mountpoint }}/docker/pi-hole-unbound/unbound/setupVars.conf
    - source: {{ files_switch(['setupVars.conf.tmpl'],
                              lookup='pi-hole-unbound-docker-setup-vars-managed',
                 )
              }}
    - user: {{ user }}
    - group: {{ group }}
    - mode: '0666'
    - makedirs: True
    - template: jinja
    - context:
        gateway: {{ gateway }}
        interface: {{ listen_interface }}
        network_cidr: {{ network_cidr }}
    - require:
      - pi-hole-unbound-docker-unbound-dir-managed
      #}
