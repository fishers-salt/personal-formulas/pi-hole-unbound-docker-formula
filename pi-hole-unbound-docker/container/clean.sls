# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import mapdata as pi_hole_unbound_docker with context %}

include:
  - {{ sls_config_clean }}

{%- set name = pi_hole_unbound_docker.container.name %}
{%- set image = pi_hole_unbound_docker.container.image %}

pi-hole-unbound-docker-container-clean-container-absent:
  docker_container.absent:
    - name: {{ name }}
    - force: True

pi-hole-unbound-docker-docker-clean-image-absent:
  docker_image.absent:
    - name: {{ image }}
    - require:
      - pi-hole-unbound-docker-container-clean-container-absent
