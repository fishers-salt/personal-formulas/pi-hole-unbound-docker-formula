# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as pi_hole_unbound_docker with context %}
{%- set sls_config_file = tplroot ~ '.config.file' %}

include:
  - {{ sls_config_file }}

{%- set os = salt.grains.get('os', none) %}
{%- if os == 'Ubuntu' or os == 'Fedora' %}
{# See https://github.com/pi-hole/docker-pi-hole#installing-on-ubuntu-or-fedora #}
pi-hole-unbound-docker-container-running-systemd-stub-disabled:
  file.replace:
    - name: /etc/systemd/resolved.conf
    - pattern: '#?DNSStubListener=yes'
    - repl: DNSStubListener=no
    - backup: orig

pi-hole-unbound-docker-container-running-resolv-conf-symlink-replaced:
  file.symlink:
    - name: /etc/resolv.conf
    - target: /run/systemd/resolve/resolv.conf
    - force: True
    - require:
      - pi-hole-unbound-docker-container-running-systemd-stub-disabled

pi-hole-unbound-docker-container-running-resolvd-restart:
  service.running:
    - name: systemd-resolved
    - enable: True
    - watch:
      - pi-hole-unbound-docker-container-running-systemd-stub-disabled
      - pi-hole-unbound-docker-container-running-resolv-conf-symlink-replaced
{%- endif %}

{%- set name = pi_hole_unbound_docker.container.name %}
{%- set image = pi_hole_unbound_docker.container.image %}
{%- set tag = pi_hole_unbound_docker.container.tag %}
{%- set mountpoint = pi_hole_unbound_docker.docker.mountpoint %}

pi-hole-unbound-docker-container-running-image-present:
  docker_image.present:
    - name: {{ image }}
    - tag: {{ tag }}
    - force: True
{%- set listen_interface = pi_hole_unbound_docker['listen_interface'] %}
{%- set interfaces = salt.grains.get('ip_interfaces', {}) %}
{%- set hostname = pi_hole_unbound_docker['hostname'] %}
{%- set domain = pi_hole_unbound_docker.container['url'] %}
{%- set theme = pi_hole_unbound_docker['theme'] %}
{%- set password = pi_hole_unbound_docker['web_password'] %}
{%- set addresses = interfaces.get(listen_interface, []) %}
{%- if addresses|length == 0 %}
{%- set address = 'test' %}
{%- else %}
{%- set address = addresses[0] %}
{%- endif %}

pi-hole-unbound-docker-container-running-container-managed:
  docker_container.running:
    - name: {{ name }}
    - image: {{ image }}:{{ tag }}
    - hostname: {{ hostname }}
    - domainname: {{ domain }}
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/pi-hole-unbound/unbound:/etc/pihole:rw
      - {{ mountpoint }}/docker/pi-hole-unbound/dnsmasq:/etc/dnsmasq.d:rw
    - port_bindings:
      - 53:53/tcp
      - 53:53/udp
    - dns:
      - 172.16.1.2
      - 192.168.0.1
    - watch:
      - sls: {{ sls_config_file }}
    - environment:
      - DHCP_ACTIVE: {{ pi_hole_unbound_docker.dhcp_active }}
      {% if pi_hole_unbound_docker.dhcp_active %}
      - DHCP_END: {{ pi_hole_unbound_docker.dhcp_end }}
      - DHCP_IPv6: {{ pi_hole_unbound_docker.dhcp_ipv6 }}
      - DHCP_LEASETIME: {{ pi_hole_unbound_docker.dhcp_leasetime }}
      - DHCP_ROUTER: {{ pi_hole_unbound_docker.dhcp_router }}
      - DHCP_START: {{ pi_hole_unbound_docker.dhcp_start }}
      - DHCP_rapid_commit: {{ pi_hole_unbound_docker.dhcp_rapid_commit }}
      {% endif %}
      - DNSMASQ_LISTENING: single
      - DNSMASQ_USER: root
      - DNSSEC: true
      - DNS_BOGUS_PRIV: {{ pi_hole_unbound_docker.dns_bogus_priv }}
      - DNS_FQDN_REQUIRED: {{ pi_hole_unbound_docker.dns_fqdn_required }}
      - FTLCONF_LOCAL_IPV4: {{ address }}
      - IPv6: {{ pi_hole_unbound_docker.ipv6 }}
      - PIHOLE_DNS_: 127.0.0.1#5335
      - PIHOLE_DOMAIN: {{ pi_hole_unbound_docker.pihole_domain }}
      - QUERY_LOGGING: {{ pi_hole_unbound_docker.query_logging }}
      - REV_SERVER: {{ pi_hole_unbound_docker.rev_server }}
      {% if pi_hole_unbound_docker.rev_server == true %}
      {% if pi_hole_unbound_docker.rev_server_domain is defined %}
      - REV_SERVER_DOMAIN: {{ pi_hole_unbound_docker.rev_server_domain }}
      {% endif %}
      {%- set gateway = salt.grains.get('ip4_gw', '192.168.0.1') %}
      - REV_SERVER_TARGET: {{ gateway }}
      - REV_SERVER_CIDR: {{ pi_hole_unbound_docker.rev_server_cidr }}
      {% endif %}
      - TEMPERATUREUNIT: {{ pi_hole_unbound_docker.temperature_unit }}
      - TZ: {{ pi_hole_unbound_docker.timezone }}
      - VIRTUAL_HOST: {{ pi_hole_unbound_docker.container.url }}
      - WEBPASSWORD: {{ password }}
      - WEBUIBOXEDLAYOUT: {{ pi_hole_unbound_docker.web_ui_boxed_layout }}
      - WEBTHEME: {{ theme }}
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.pihole-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.pihole-web.middlewares=pihole-redirect-websecure"
      - "traefik.http.routers.pihole-web.rule=Host(`{{ pi_hole_unbound_docker.container.url }}`)"
      - "traefik.http.routers.pihole-web.entrypoints=web"
      - "traefik.http.routers.pihole-websecure.rule=Host(`{{ pi_hole_unbound_docker.container.url }}`)"
      - "traefik.http.routers.pihole-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.pihole-websecure.tls=true"
      - "traefik.http.routers.pihole-websecure.entrypoints=websecure"
      - "traefik.http.services.pihole.loadBalancer.server.port=80"
      - "traefik.http.middlewares.pihole-admin.addprefix.prefix=/admin"
    {%- if os == 'Ubuntu' or os == 'Fedora' %}
    - require:
      - pi-hole-unbound-docker-container-running-systemd-stub-disabled
      - pi-hole-unbound-docker-container-running-resolv-conf-symlink-replaced
      - pi-hole-unbound-docker-container-running-resolvd-restart
      - sls: {{ sls_config_file }}
    {%- endif %}
